//
//  BlogCell.swift
//  IHerb
//
//  Created by Admin on 20.06.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class BlogCell: UITableViewCell {
    
    @IBOutlet var rootView : UIView!
    @IBOutlet var titleLabel : UILabel!
//    @IBOutlet var descLabel : UILabel!
    @IBOutlet var imageRoot : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
/*
{
   "article_√": "https://appserver-id30.site/pics/art1.jpg",
   "article_title": "Свежие купоны на май-июнь 2020",
   "article_text": "XEN20 - Скидка 30% на Xenadrine. Скидка применяется в корзине. Не суммируется с другими скидками. \nAFG1283 - скидка 10% на первый заказ. \nБесплатная доставка при заказе от $60"
},
*/
