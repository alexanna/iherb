//
//  FeedbackViewController.swift
//  IHerb
//
//  Created by Admin on 20.06.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var emailFileld: UITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround() 
        textView.layer.cornerRadius = 5
        textView.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        textView.layer.borderWidth = 0.5
        textView.clipsToBounds = true
        
        emailFileld.attributedPlaceholder = NSAttributedString(string:"Ваш email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        sendButton.layer.cornerRadius = 5

    }
    
    func setClear() {
        textView.text = ""
        emailFileld.text = ""
    }
    
    @IBAction func showAlertButtonTapped(_ sender: UIButton) {
        self.setClear()
          // create the alert
          let alert = UIAlertController(title: "Отзыв отправлен", message: "Спасибо за отзыв! Мы свяжемся с вами если потребуются детали", preferredStyle: UIAlertController.Style.alert)

          // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Ок", style: UIAlertAction.Style.cancel, handler: { action in
//            self.setClear()
        }))

          // show the alert
          self.present(alert, animated: true, completion: nil)
      }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)

        textView.delegate = self
        textView.text = "Ваш отзыв"
        textView.textColor = UIColor.lightGray

    }
    


    // MARK: - Place Holder
    func textViewDidBeginEditing(_ textView: UITextView)
    {

        if !textView.text!.isEmpty && textView.text! == "Ваш отзыв"
        {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView)
    {

        if textView.text.isEmpty
        {
            textView.text = "Ваш отзыв"
            textView.textColor = UIColor.lightGray
        }
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
