//
//  ViewController.h
//  ApiDemo
//
//  Created by Dimmetrius on 14.01.16.
//  Copyright © 2016 JivoSite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JivoSdk.h"
#import <WebKit/WebKit.h>

@interface ChatController : UIViewController<JivoDelegate,UIWebViewDelegate, WKNavigationDelegate, WKUIDelegate>


@end

