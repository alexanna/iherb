//
//  ChatViewController.swift
//  IHerb
//
//  Created by Admin on 21.06.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import WebKit

class ChatViewController: UIViewController, WKUIDelegate {

    @IBOutlet weak var webView: WKWebView!
    var webContentView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.uiDelegate = self
//        let webConfiguration = WKWebViewConfiguration()
//        webConfiguration.ignoresViewportScaleLimits = true
//        webConfiguration.suppressesIncrementalRendering = true
//        webConfiguration.allowsInlineMediaPlayback = true
//        webConfiguration.allowsAirPlayForMediaPlayback = false
//        webConfiguration.allowsPictureInPictureMediaPlayback = true
//        webConfiguration.mediaTypesRequiringUserActionForPlayback = .all
//        webConfiguration.requiresUserActionForMediaPlayback = true
//        webView = WKWebView(frame: webView.frame, configuration: webConfiguration)
        
        
//        let config = WKWebViewConfiguration()
//        config.dataDetectorTypes = [.all]
//        let webView = WKWebView(frame: .zero, configuration: config)
        
        
        let url = URL(string: "https://appserver-id30.site/chat.html")!
        webView.load(URLRequest(url: url))
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
