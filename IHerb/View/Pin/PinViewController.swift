//
//  PinViewController.swift
//  IHerb
//
//  Created by UjiN on 6/22/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import UserNotifications
import PhoneNumberKit

class PinViewController: UIViewController, UITextFieldDelegate {
	@IBOutlet weak var numberTextField: TextField!
	@IBOutlet weak var codeTextField: TextField!
	@IBOutlet weak var numberLabel: UILabel!
	@IBOutlet weak var codeLabel: UILabel!
	@IBOutlet weak var sendButton: UIButton!
	@IBOutlet weak var confirmButton: UIButton!
	var pinCode: Bool = false
	var phoneCode: String = ""
	let notificationCenter = UNUserNotificationCenter.current()
	let phoneNumberKit = PhoneNumberKit()
	var notificationCode: String = ""
	
	var countryCode: String {
		return Locale.current.regionCode ?? "US"
	}
	
	var showTabBar: (() -> Void)?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		notificationCenter.delegate = self
		notificationCenter.requestAuthorization(options: [.badge, .sound, .alert]) { (granted, error) in
			guard granted else { return }
			self.notificationCenter.getNotificationSettings { (settings) in
				print(settings)
				guard settings.authorizationStatus == .authorized else { return }
			}
		}
		
		numberTextField.delegate = self
		codeTextField.delegate = self
		sendButton.layer.cornerRadius = 5
		confirmButton.layer.cornerRadius = 5
		numberTextField.layer.borderWidth = 2
		numberTextField.layer.cornerRadius = 5
		codeTextField.layer.borderWidth = 2
		codeTextField.layer.cornerRadius = 5
		numberTextField.layer.borderColor = UIColor.lightGray.cgColor
		numberLabel.textColor = UIColor.darkGray
		codeTextField.layer.borderColor = UIColor.lightGray.cgColor
		codeLabel.textColor = UIColor.darkGray
		
		if let code = phoneNumberKit.countryCode(for: countryCode) {
			phoneCode = "\(code)"
		}
		numberTextField.text = "+" + phoneCode
	}
	
	@IBAction func sendCode(_ sender: UIButton) {
		
		let numberCount = phoneNumberKit.metadata(for: countryCode)?.mobile?.exampleNumber?.count ?? 10
		let count = numberCount + phoneCode.count
		let digitNumber = numberTextField.text?.digits
		if digitNumber?.count != count {
			let alert = UIAlertController(title: "Ошибка!", message: "Такого номера не существует", preferredStyle: UIAlertController.Style.alert)
			alert.addAction(UIAlertAction(title: "ОК", style: UIAlertAction.Style.default, handler: nil))
			self.present(alert, animated: true, completion: nil)
			return
		}
		
		sendNotifications()
	}
	
	
	@IBAction func confirmCode(_ sender: UIButton) {
		if codeTextField.text != notificationCode {
			let alert = UIAlertController(title: "Ошибка!", message: "Неверный код", preferredStyle: UIAlertController.Style.alert)
			alert.addAction(UIAlertAction(title: "ОК", style: UIAlertAction.Style.default, handler: nil))
			self.present(alert, animated: true, completion: nil)
			return
		}
		let defaults = UserDefaults.standard
		defaults.set(true, forKey: "Pincode")
		showTabBar?()
	}
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		let text = (textField.text ?? "") as NSString
		let resultString = text.replacingCharacters(in: range, with: string)
		if textField == numberTextField {
			let numberCount = phoneNumberKit.metadata(for: countryCode)?.mobile?.exampleNumber?.count ?? 10
			let count = numberCount + phoneCode.count
			let digitNumber = resultString.digits
			let prefix = "+" + phoneCode
			if resultString.hasPrefix(prefix) && digitNumber.count <= count {
				return true
			} else {
				return false
			}
		}
		return true
	}
	
	func textFieldDidBeginEditing(_ textField: UITextField) {
		textField.layer.borderColor = UIColor(named: "iHerbColor")?.cgColor
		if textField == numberTextField {
			numberLabel.textColor = UIColor(named: "iHerbColor")
		} else if textField == codeTextField {
			codeLabel.textColor = UIColor(named: "iHerbColor")
		}
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		textField.layer.borderColor = UIColor.lightGray.cgColor
		if textField == numberTextField {
			numberLabel.textColor = UIColor.darkGray
		} else if textField == codeTextField {
			codeLabel.textColor = UIColor.darkGray
		}
	}
	
	func sendNotifications() {
		let content = UNMutableNotificationContent()
		content.title = "Введите код подтверждения"
		notificationCode = PhoneHelper.generateRandomDigits(4)
		content.body = notificationCode
		content.sound = UNNotificationSound.default
		
		let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
		
		let request = UNNotificationRequest(identifier: "notification", content: content, trigger: trigger)
		notificationCenter.add(request) { (error) in
		}
	}
}

extension PinViewController: UNUserNotificationCenterDelegate {
	func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
		completionHandler([.alert, .sound])
	}
	
	func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
		codeTextField.text = notificationCode
	}
}
