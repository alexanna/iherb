//
//  ShopViewController.swift
//  IHerb
//
//  Created by UjiN on 6/21/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import WebKit

class ShopViewController: UIViewController, WKNavigationDelegate {
	
	var webView: WKWebView!
	var shopUrlString: String?
	
	private var activityIndicatorContainer: UIView!
	private var activityIndicator: UIActivityIndicatorView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		if let shopUrl = shopUrlString,
			let url = URL(string: shopUrl) {
			webView.load(URLRequest(url: url))
		}

		navigationController?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "coupon"), style: .plain, target: self, action: #selector(goBack))
		navigationController?.navigationItem.leftBarButtonItem?.tintColor = .clear
		navigationController?.navigationItem.leftBarButtonItem?.isEnabled = false
	}
	
	override func loadView() {
		webView = WKWebView()
		webView.navigationDelegate = self
		view = webView
	}
	
	@objc private func goBack() {
		if webView.canGoBack {
			webView.goBack()
		}
	}
	
	func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
		decisionHandler(.allow)
	}
	
	func webView(webView: WKWebView!, createWebViewWithConfiguration configuration: WKWebViewConfiguration!, forNavigationAction navigationAction: WKNavigationAction!, windowFeatures: WKWindowFeatures!) -> WKWebView! {
		if navigationAction.targetFrame == nil {
			webView.load(navigationAction.request)
		}
		return nil
	}
	
	func webView(_ webView: WKWebView,
		didReceive challenge: URLAuthenticationChallenge,
		completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void)
	{
		if(challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust)
		{
			let cred = URLCredential(trust: challenge.protectionSpace.serverTrust!)
			completionHandler(.useCredential, cred)
		}
		else
		{
			completionHandler(.performDefaultHandling, nil)
		}
	}
	
	fileprivate func setActivityIndicator() {
		// Configure the background containerView for the indicator
		activityIndicatorContainer = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
		activityIndicatorContainer.center.x = webView.center.x
		// Need to subtract 44 because WebKitView is pinned to SafeArea
		//   and we add the toolbar of height 44 programatically
		activityIndicatorContainer.center.y = webView.center.y - 44
		activityIndicatorContainer.backgroundColor = UIColor.black
		activityIndicatorContainer.alpha = 0.8
		activityIndicatorContainer.layer.cornerRadius = 10
	  
		// Configure the activity indicator
		activityIndicator = UIActivityIndicatorView()
		activityIndicator.hidesWhenStopped = true
		activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
		activityIndicator.translatesAutoresizingMaskIntoConstraints = false
	activityIndicatorContainer.addSubview(activityIndicator)
		webView.addSubview(activityIndicatorContainer)
		
		// Constraints
		activityIndicator.centerXAnchor.constraint(equalTo: activityIndicatorContainer.centerXAnchor).isActive = true
		activityIndicator.centerYAnchor.constraint(equalTo: activityIndicatorContainer.centerYAnchor).isActive = true
	  }
	
	fileprivate func showActivityIndicator(show: Bool) {
		if show {
		  activityIndicator.startAnimating()
		} else {
		  activityIndicator.stopAnimating()
		  activityIndicatorContainer.removeFromSuperview()
		}
	  }


	func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
	  // Set the indicator everytime webView started loading
	  self.setActivityIndicator()
	  self.showActivityIndicator(show: true)
	}
	
	func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
	  self.showActivityIndicator(show: false)
	}
	
	func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
		navigationController?.navigationItem.leftBarButtonItem?.tintColor = webView.canGoBack ? .white : .clear
		navigationController?.navigationItem.leftBarButtonItem?.isEnabled = webView.canGoBack
		self.showActivityIndicator(show: false)
	}
}
