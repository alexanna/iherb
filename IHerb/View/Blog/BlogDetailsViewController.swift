//
//  BlogDetailsViewController.swift
//  IHerb
//
//  Created by Admin on 21.06.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class BlogDetailsViewController: UIViewController {
    
    var myArticle: Articles?
    @IBOutlet weak var imageRoot: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        print(myArticle)
        self.title = myArticle?.articleTitle
        
        let stringTest = (myArticle?.articlePic)!
        let url = URL(string: stringTest.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)
        imageRoot.af.setImage(withURL: url!)
        
        titleLabel.text = myArticle?.articleTitle
        descTextView.text = myArticle?.articleText
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
