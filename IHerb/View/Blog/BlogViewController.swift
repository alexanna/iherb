//
//  BlogViewController.swift
//  IHerb
//
//  Created by Admin on 20.06.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class BlogViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView : UITableView!
    var myConfig: Config?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 77)
        tableView.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9803921569, blue: 0.9882352941, alpha: 1)//F9FAFC
        
        print(myConfig?.articles)
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myConfig?.articles?.count ?? 0 //myLoans?.loans.count ?? 0
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
             let cell = tableView.dequeueReusableCell(withIdentifier: "blogCell", for: indexPath) as! BlogCell
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            cell.rootView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)//FFFFFF
            
            cell.rootView.layer.masksToBounds = false
            cell.rootView.layer.shadowOffset = CGSize(width: -1, height: 1)
            cell.rootView.layer.shadowRadius = 1
            cell.rootView.layer.shadowOpacity = 0.5
            
            cell.titleLabel.text = myConfig?.articles?[indexPath.row].articleTitle
            
            let stringTest = (myConfig?.articles?[indexPath.row].articlePic)!
            let url = URL(string: stringTest.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)
            cell.imageRoot.af.setImage(withURL: url!)

            cell.rootView.layer.cornerRadius =  15
            
            return cell
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 110//549
        }
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Blog", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "BlogDetails") as! BlogDetailsViewController
            newViewController.myArticle = myConfig?.articles?[indexPath.row]
            self.navigationController?.show(newViewController, sender: true)
        }
    

}

//cell.rootView.layer.shadowColor = UIColor.black.cgColor
//cell.rootView.layer.shadowOpacity = 0.5
//cell.rootView.layer.shadowOffset = .zero
//cell.rootView.layer.shadowRadius = 50
//cell.rootView.layer.shadowPath = UIBezierPath(rect: cell.rootView.bounds).cgPath
//cell.rootView.layer.shouldRasterize = true
//cell.rootView.layer.rasterizationScale = UIScreen.main.scale
