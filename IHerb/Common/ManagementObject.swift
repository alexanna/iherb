//
//  ManagementObject.swift
//  IHerb
//
//  Created by Admin on 20.06.2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import Alamofire

class ManagementObject {
	
	var myConfig: Config?
	let pinCode = UserDefaults.standard.bool(forKey: "Pincode")
	
	func setTabBar(window : UIWindow) {
		
		//                            let navigationBarAppearace = UINavigationBar.appearance()
		//                          navigationBarAppearace.tintColor = .white
		//                          navigationBarAppearace.barTintColor = UIColor(named: "redTest")
		//                          navigationBarAppearace.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white ]
		//                          UINavigationBar.appearance().isTranslucent = false
		//
		//                          UIApplication.shared.statusBarStyle = .lightContent
		
		let tabBarController = UITabBarController()
		
		if let shop = myConfig?.shop, !shop.isEmpty {
			let shopStoryboard = UIStoryboard(name: "Shop", bundle: nil)
			let navigation = shopStoryboard.instantiateViewController(withIdentifier: "navigation") as! UINavigationController
			let shopVC = navigation.viewControllers[0] as! ShopViewController
			shopVC.shopUrlString = myConfig?.shop
			tabBarController.addChild(navigation)
		}
		
		let blogStoryboard = UIStoryboard(name: "Blog", bundle: nil)
		let blogViewController = blogStoryboard.instantiateInitialViewController() as! UINavigationController
		(blogViewController.viewControllers.first as! BlogViewController).myConfig = myConfig
		tabBarController.addChild(blogViewController)
		
		let chatStoryboard = UIStoryboard(name: "Chat", bundle: nil)
		let chatSViewController = chatStoryboard.instantiateInitialViewController()!
		tabBarController.addChild(chatSViewController)
		
		//		myConfig?.coupon = "https://www.google.com/"
		if let coupon = myConfig?.coupon, !coupon.isEmpty {
			let shopStoryboard = UIStoryboard(name: "Shop", bundle: nil)
			let navigation = shopStoryboard.instantiateViewController(withIdentifier: "navigation") as! UINavigationController
			navigation.tabBarItem.image = UIImage(named: "coupon")
			let shopVC = navigation.viewControllers[0] as! ShopViewController
			shopVC.shopUrlString = myConfig?.coupon
			shopVC.title = "Купон"
			tabBarController.addChild(navigation)
		}
		
		let feedbackStoryboard = UIStoryboard(name: "Feedback", bundle: nil)
		let feedbackViewController = feedbackStoryboard.instantiateInitialViewController()!
		tabBarController.addChild(feedbackViewController)
		
		tabBarController.tabBar.barTintColor = UIColor(named: "iHerbColor")
		tabBarController.tabBar.tintColor = UIColor(named: "tabBarSelected")
		tabBarController.tabBar.unselectedItemTintColor = UIColor(named: "tabBarNotSelected")
		
		window.rootViewController = tabBarController
		window.backgroundColor = .white
	}
	
	func rootPin(window: UIWindow) {
		let pinStoryboard = UIStoryboard(name: "Pin", bundle: nil)
		let pinSukaViewController = pinStoryboard.instantiateInitialViewController() as! PinViewController
		pinSukaViewController.showTabBar = {
			self.setTabBar(window: window)
		}
			window.rootViewController = pinSukaViewController
	}
	
	
	func getRequest(window : UIWindow) {
		AF.request("https://appserver-id21.site/data.json")
			.responseDecodable(of: Config.self) { (response) in
				guard let backConfig = response.value else { return }
				self.myConfig = backConfig
				print(self.myConfig!)
				if let pin = backConfig.pin, pin > 100000 && !self.pinCode {
					self.rootPin(window: window)
				} else {
					self.setTabBar(window: window)
				}
		}
	}
}
