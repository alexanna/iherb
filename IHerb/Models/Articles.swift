//
//  Articles.swift
//  IHerb
//
//  Created by UjiN on 6/20/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct Articles: Codable {
	var articlePic: String?
	var articleTitle: String?
	var articleText: String?
	
	enum CodingKeys: String, CodingKey {
		case articlePic = "article_pic"
		case articleTitle = "article_title"
		case articleText = "article_text"
	}
}
