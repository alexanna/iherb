//
//  Config.swift
//  IHerb
//
//  Created by UjiN on 6/20/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct Config: Codable {
	let articles: [Articles]?
	var pin: Int?
	var shop: String?
	var coupon: String?
	var chat: String?
}
