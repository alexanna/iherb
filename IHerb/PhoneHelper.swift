//
//  PhoneHelper.swift
//  IHerb
//
//  Created by UjiN on 6/25/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class PhoneHelper {
	
	class func generateRandomDigits(_ digitNumber: Int) -> String {
		var number = ""
		for i in 0..<digitNumber {
			var randomNumber = arc4random_uniform(10)
			while randomNumber == 0 && i == 0 {
				randomNumber = arc4random_uniform(10)
			}
			number += "\(randomNumber)"
		}
		return number
	}
}
extension String {
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }
}
